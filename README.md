# mundodevopstest

### SOLUÇÃO

Projeto criado com o comando `react-native init mundodevopstest` utilizando a versão `0.58.3`. Segue as definições do [PDF](mundodevopstest-files/Desafio.pdf), o layout do [sketch](mundodevopstest-files/sketch.png) e das propriedades de cores e da fonte.

Roda em dispositivos Apple (iOS) e em Android.

### FUNCIONAMENTO

O projeto é responsivo, independente da orientação e do SO do dispositivo.

Se nenhum filtro estiver habilitado todos os exercício serão exibidos. É possível selecionar mais de um filtro, porém, se um deles não obter exercício cadastrado, não aparecerá na lista, exemplo o filtro de Dança.

Em cada exercício encontra-se botões “Hoje” e “Ontem” para informar que o mesmo foi concluído.

### INSTALAÇÃO

Requisitos:

- [NodeJS](https://nodejs.org/en/download/)
- [React-native](https://facebook.github.io/react-native/docs/getting-started)
- **XCode**<sup>[1]</sup>, ou o **Android Studio**<sup>[2]</sup>

Primeiro, baixar e instalar o NodeJS na versão LTS.
Em seguida, rodar o comando `npm install -g react-native-cli` no terminal para instalar o comando globalmente do react-native.

<details>
<summary>Instalação para dispositivos iOS</summary>

Após a conclusão do comando anterior, deve-se baixar e instalar o xcode<sup>\*</sup> com o `command-line tools` instalado.

<sup>\*</sup> Para mais informações, abrir a documentação do react-native abaixo.

</details>

<details>
<summary>Instalação para dispositivos Android</summary>

Após a conclusão do comando anterior, é necessário baixar e instalar o Android Studio e também criar um emulador<sup>\*</sup>.

<sup>\*</sup> Para mais informações, abrir a documentação do react-native abaixo.

</details>

[Documentação do React-Native](https://facebook.github.io/react-native/)

> <sup>[1]</sup> Somente para máquinas Mac

> <sup>[2]</sup> Para máquinas Mac/Windows/Linux

### COMO RODAR

Após obter o repositório do projeto, por clone (com o comando `git clone https://gitlab.com/filipepiresg/mundodevopstest`) ou download, deverá ser feito o download das dependências do projeto através do comando `npm install` no terminal.

Se tudo estiver dado certo, o projeto já deve estar funcionando perfeitamente e para rodar tem que utilizar o comando `react-native run-ios` ou `react-native run-android` para dispositivos iOS e android, respectivamente.

 <p style="color:red; text-align:center;font-weight:bold">Caso seja utilizado o android, é necessário abrir o emulador antes de rodar o projeto</p>

Após ter instalado no dispositivo, pela primeira vez, não é necessário rodar esse comando (pois demora demais) podemos simplesmente rodar o comando `react-native start` para que o bundler inicie rapidamente.

### SCREENSHOTS

Segue alguns screenshots de dispositivos da Apple e Android:

![iPhone 5S](screenshots/iPhone5S.png "iPhone 5S")

> Resolução 640x1136

![iPhone XR](screenshots/iPhoneXR.png "iPhone XR")

> Resolução 828x1792

![Android Pixel2](screenshots/AndroidPixel2.png "Android Pixel 2")

> Resolução 1920x1080 com left rotation

### PROPRIEDADES

<details>

- `#FEFFFF` (COR TEXTO)
- `#7F38F4` -> `#F22B48` (COR GRADIENTE)
- `#19B996` (COR BOTÃO ONTEM)
- `#FD3C29` (COR BOTÃO HOJE)
- `#262F38` (COR BACKGROUND)
- `#323C47` (COR SECUNDÁRIA)
- `Montserrat` (Família fonte)
- [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons) (PACOTE DE ICONES)
- [imagens](mundodevopstest-files/img) (PACOTE DE IMAGENS)
- [data.json](mundodevopstest-files/data.json)
- [sketch](mundodevopstest-files/sketch.png)
- [Desafio](mundodevopstest-files/Desafio.pdf)
- `react-native --version` = `react-native: 0.58.3`

</details>

---
