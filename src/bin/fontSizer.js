import { Dimensions } from "react-native"

export default (
  width = Number || Dimensions.get("window").width,
  sizeMax = Number
) => {
  if (width > 400) return sizeMax
  else if (width > 250) return sizeMax - 4
  return sizeMax - 6
}
