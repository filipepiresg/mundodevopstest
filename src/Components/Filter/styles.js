import { Dimensions } from "react-native"
const { height } = Dimensions.get("window")

import globalStyles from "../../Styles"

const styles = {
  container: {
    backgroundColor: globalStyles.colorItems,
    borderRadius: (height * 0.1) / 6,
    marginBottom: 15,
    paddingVertical: 10
  },
  list: {
    alignItems: "center",
    paddingHorizontal: 20
  }
}

export default styles
