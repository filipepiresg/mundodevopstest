import React, { Component } from "react"

import { FlatList, View } from "react-native"

import styles from "./styles"
import { filters } from "../../../mundodevopstest-files/data.json"
import BtnFilter from "../BtnFilter"

export default class Filter extends Component {
  _renderItem = ({ item: { name, image } }) => {
    const { filtered, handleFilter } = this.props
    const isActive = filtered.includes(name)
    return (
      <BtnFilter
        name={name}
        image={image}
        isActive={isActive}
        handleFilter={handleFilter}
      />
    )
  }

  _separator = () => <View style={{ paddingHorizontal: 10 }} />

  render() {
    return !filters ? (
      <View />
    ) : (
      <View style={styles.container}>
        <FlatList
          horizontal
          data={filters}
          extraData={this.props}
          renderItem={this._renderItem}
          contentContainerStyle={styles.list}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          ItemSeparatorComponent={this._separator}
          keyExtractor={(item, index) => item.name + "" + index.toString()}
        />
      </View>
    )
  }
}
