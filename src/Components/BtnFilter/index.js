import React, { Component } from "react"

import { Image, TouchableOpacity, Platform } from "react-native"
import LinearGradient from "react-native-linear-gradient"

import styles from "./styles"
import globalStyles from "../../Styles"
import Check from "../Check"

export default class BtnFilter extends Component {
  constructor(props) {
    super(props)
    this.handleFilter = this.props.handleFilter
  }

  _handlePathImage = (pathImage = String) => {
    console.log(pathImage)
    if (Platform.OS === "android") return `asset:/${pathImage}`
    let path = pathImage.replace("img/", "")
    path = path.replace(".png", "")
    return path || "ic_yoga"
  }

  render() {
    const { image, name, isActive } = this.props

    return image === null ? null : (
      <TouchableOpacity onPress={async () => await this.handleFilter(name)}>
        <LinearGradient
          start={{ x: 0.0, y: 0.25 }}
          end={{ x: 0.75, y: 1.3 }}
          locations={[0, 1]}
          colors={[
            globalStyles.colorGradiente.inicio,
            globalStyles.colorGradiente.fim
          ]}
          style={styles.linearGradient}
        >
          <Image
            source={{ uri: this._handlePathImage(image) }}
            style={{ height: "60%", width: "60%" }}
            resizeMode="contain"
          />
        </LinearGradient>
        <Check active={isActive} />
      </TouchableOpacity>
    )
  }
}
