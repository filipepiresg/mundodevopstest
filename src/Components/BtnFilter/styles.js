import { Dimensions } from "react-native"
const { width } = Dimensions.get("window")

const styles = {
  linearGradient: {
    borderRadius: width / 14,
    width: width / 7,
    height: width / 7,
    alignItems: "center",
    justifyContent: "center"
  }
}

export default styles
