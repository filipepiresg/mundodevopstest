import React, { Component } from "react"

import { View, Text, Image, Platform } from "react-native"

import styles from "./styles"
import globalStyles from "../../Styles"
import ButtonDay from "../ButtonDay"
import Info from "../Infos"

export default class ExerciceItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isToday: false,
      isYesterday: false
    }
  }

  componentDidMount() {
    const { when } = this.props.item
    if (when && (when === "today" || when === "yesterday")) {
      this._handleWhen(when)
    }
  }

  _handleWhen = (when = "today" || "yesterday") => {
    const { isToday, isYesterday } = this.state
    if (when === "today") {
      this.setState({ isToday: !isToday })
    } else {
      this.setState({ isYesterday: !isYesterday })
    }
  }

  _handleButtonToday = () => {
    const { isToday } = this.state
    this.setState({ isToday: !isToday })
  }
  _handleButtonYesterday = () => {
    const { isYesterday } = this.state
    this.setState({ isYesterday: !isYesterday })
  }

  _handlePathImage = (pathImage = String) => {
    if (Platform.OS === "android") return `asset:/${pathImage}`
    let path = pathImage.replace("img/", "")
    path = path.replace(".png", "")
    return path || "ic_yoga"
  }

  render() {
    const { name, calories, time, weight, image } = this.props.item
    const { isToday, isYesterday } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.containerImage}>
          <Image
            source={{ uri: this._handlePathImage(image) }}
            style={{ height: "100%", width: "100%" }}
            resizeMode="contain"
          />
        </View>
        <View style={styles.otherContainer}>
          <View style={styles.containerInfo}>
            <Text style={styles.txtExercices}>{name.toUpperCase()}</Text>
          </View>
          <View style={styles.containerInfo}>
            <Info type="calories">{calories}</Info>
            <Info type="time" separator>
              {time}
            </Info>
            <Info type="weight">{weight}</Info>
          </View>
          <View style={styles.containerButton}>
            <ButtonDay
              isActive={isToday}
              onPress={this._handleButtonToday}
              color={globalStyles.colorBtnToday}
            >
              HOJE
            </ButtonDay>
            <ButtonDay
              isActive={isYesterday}
              onPress={this._handleButtonYesterday}
              color={globalStyles.colorBtnYesterday}
            >
              ONTEM
            </ButtonDay>
          </View>
        </View>
      </View>
    )
  }
}
