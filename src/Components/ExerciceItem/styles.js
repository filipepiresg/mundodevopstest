import { Dimensions } from "react-native"

import globalStyles from "../../Styles"
import fontSizer from "../../bin/fontSizer"
const { height, width } = Dimensions.get("window")

const styles = {
  container: {
    paddingHorizontal: width > 300 ? 20 : 10,
    paddingVertical: 10,
    borderRadius: height / 50,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: globalStyles.colorItems,
    marginVertical: 15
  },
  containerImage: {
    height: width / 4,
    width: width / 4,
    borderRadius: width / 8,
    backgroundColor: globalStyles.colorBackground
  },
  otherContainer: {
    flex: 1,
    marginLeft: width > 300 ? 20 : 10,
    justifyContent: "center"
  },
  txtExercices: {
    fontFamily: globalStyles.fontFamily,
    color: globalStyles.colorText,
    fontSize: fontSizer(width, 20)
  },
  containerButton: {
    flexDirection: "row"
  },
  containerInfo: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingBottom: 15
  }
}

export default styles
