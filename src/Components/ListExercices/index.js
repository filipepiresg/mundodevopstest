import React from "react"

import { FlatList, View } from "react-native"

import ExercicesItem from "../ExerciceItem"
import { exercices } from "../../../mundodevopstest-files/data.json"

const _renderItem = (item, filtered = [String]) => {
  const { name } = item
  if (filtered && filtered.length > 0) {
    return !filtered.includes(_verifyItem(name)) ? (
      <View />
    ) : (
      <ExercicesItem item={item} />
    )
  }
  return <ExercicesItem item={item} />
}

_verifyItem = name => {
  switch (name) {
    case "Yoga":
      return "Yoga"
    case "Musculação":
      return "Upper Body"
    case "Bicicleta":
      return "Lower Body"
    case "Corrida":
      return "Lower Body"
    default:
      return null
  }
}

const ListExercices = props =>
  exercices.length > 0 ? (
    <FlatList
      data={exercices}
      extraData={props}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      keyExtractor={(item, index) => index + "" + item.name}
      renderItem={({ item }) => _renderItem(item, props.filtered)}
    />
  ) : (
    <View />
  )

export default ListExercices
