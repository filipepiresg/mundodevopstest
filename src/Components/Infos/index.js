import React from "react"

import { View, Text, Image, Dimensions } from "react-native"

import globalStyles from "../../Styles"
import fontSizer from "../../bin/fontSizer"

const { width } = Dimensions.get("window")

const _handleSeparator = separator =>
  !separator ? (
    <Text />
  ) : (
    <Text
      style={{
        color: globalStyles.colorBtnYesterday,
        paddingHorizontal: width > 300 ? 6 : 4
      }}
    >
      |
    </Text>
  )
const _handleImage = type => {
  switch (type) {
    case "time":
      return require("../../../mundodevopstest-files/img/ic_time.png")
    case "weight":
      return require("../../../mundodevopstest-files/img/ic_balance.png")
    default:
      return require("../../../mundodevopstest-files/img/ic_bike.png")
  }
}
const _handleType = (value, type) => {
  switch (type) {
    case "weight":
      return value + " Kg"
    case "time": {
      if (value % 60 === 0) return value / 60 + " h"
      return value + " m"
    }
    default:
      return value + " Kcal"
  }
}

const Infos = ({
  children,
  type = ["calories", "time", "weight"],
  separator = false
}) => {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      {_handleSeparator(separator)}
      <Image source={_handleImage(type)} />
      <Text
        style={{
          fontFamily: globalStyles.fontFamily,
          color: globalStyles.colorText,
          fontSize: fontSizer(width, 13)
        }}
      >
        {" " + _handleType(children, type)}
      </Text>
      {_handleSeparator(separator)}
    </View>
  )
}

export default Infos
