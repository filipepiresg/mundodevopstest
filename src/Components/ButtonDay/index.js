import React from "react"

import { TouchableOpacity, Text } from "react-native"

import globalStyles from "../../Styles"
import styles from "./styles"

const ButtonDay = ({ children, isActive, color, onPress }) => (
  <TouchableOpacity
    activeOpacity={0.8}
    style={[
      styles.containerButton,
      {
        backgroundColor: isActive ? color : "transparent",
        borderColor: !isActive ? globalStyles.colorText : "transparent"
      }
    ]}
    onPress={onPress}
  >
    <Text style={styles.textButton}>{children}</Text>
  </TouchableOpacity>
)

export default ButtonDay
