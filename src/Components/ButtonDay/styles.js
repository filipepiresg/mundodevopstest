import { Dimensions } from "react-native"

import fontSizer from "../../bin/fontSizer"
import globalStyles from "../../Styles"

const { width } = Dimensions.get("window")

const styles = {
  textButton: {
    fontFamily: globalStyles.fontFamily,
    color: globalStyles.colorText,
    fontSize: fontSizer(width, 14),
    textAlign: "center"
  },
  containerButton: {
    paddingHorizontal: width > 400 ? 20 : 10,
    paddingVertical: width > 400 ? 2 : 1,
    borderWidth: 1,
    justifyContent: "center",
    marginRight: 10,
    borderRadius: 17
  }
}

export default styles
