import React from "react"

import { View, Dimensions } from "react-native"
import Icon from "react-native-vector-icons/MaterialIcons"

const { fontScale } = Dimensions.get("window")

import globalStyles from "../../Styles"

const Check = ({ active }) =>
  !active ? null : (
    <View
      style={{
        borderRadius: fontScale * 10,
        backgroundColor: globalStyles.colorText,
        position: "absolute",
        right: 1,
        top: 1
      }}
    >
      <Icon name="check-circle" color="green" size={fontScale * 20} />
    </View>
  )

export default Check
