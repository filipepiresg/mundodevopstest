import { Dimensions } from "react-native"

import fontSizer from "../../bin/fontSizer"
import globalStyles from "../../Styles"

const { width } = Dimensions.get("window")

const styles = {
  containerMenu: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 20
  },
  textMenu: {
    color: globalStyles.colorText,
    fontSize: fontSizer(width, 20),
    fontWeight: "300",
    fontFamily: globalStyles.fontFamily
  },
  line: {
    borderColor: globalStyles.colorItems,
    borderWidth: 1,
    marginBottom: 20
  }
}

export default styles
