import React from "react"

import { View, Text, TouchableOpacity } from "react-native"

import Icon from "react-native-vector-icons/Ionicons"

import styles from "./styles"
import globalStyles from "../../Styles"

const Menu = () => (
  <>
    <View style={styles.containerMenu}>
      <TouchableOpacity>
        <Icon name="ios-menu" size={30} color={globalStyles.colorText} />
      </TouchableOpacity>
      <Text style={styles.textMenu}>MEU PERFIL</Text>
      <TouchableOpacity>
        <Icon name="ios-cog" size={30} color={globalStyles.colorText} />
      </TouchableOpacity>
    </View>
    <View style={styles.line} />
  </>
)

export default Menu
