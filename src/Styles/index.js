const styles = {
  colorText: "#FEFFFF",
  colorGradiente: { inicio: "#7F38F4", fim: "#F22B48" },
  colorBackground: "#262F38",
  colorBtnToday: "#FD3C29",
  colorBtnYesterday: "#19B996",
  colorItems: "#323C47",
  fontFamily: "Montserrat"
}

export default styles
