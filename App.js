import React, { Component } from "react"

import { View, StyleSheet, SafeAreaView, StatusBar } from "react-native"

import globalStyles from "./src/Styles"
import { Menu, ListExercices, Filter } from "./src/Components"

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      filtered: ["Yoga", "Lower Body"]
    }
  }

  handleFilter = async (exercice = String) => {
    const { filtered } = this.state
    if (filtered.includes(exercice)) {
      filtered.map((item, index) => {
        if (item === exercice) {
          filtered.splice(index, 1)
        }
      })
    } else {
      filtered.push(exercice)
    }
    await this.setState({ filtered })
  }

  render() {
    const { filtered } = this.state
    return (
      <SafeAreaView style={styles.containerApp}>
        <View style={styles.container}>
          <Menu />
          <Filter handleFilter={this.handleFilter} filtered={filtered} />
          <ListExercices filtered={filtered} />
        </View>
        <StatusBar barStyle="light-content" />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  containerApp: {
    flex: 1,
    backgroundColor: globalStyles.colorBackground
  },
  container: {
    flex: 1,
    paddingBottom: 20,
    paddingHorizontal: 20
  }
})
